import home from './components/home';
// import drawer from './components/drawer.vue';
import prices from './components/prices'
import pictures from './components/pictures'

export default[
  { path: '/', component: home},
  { path: '/home', component: home},
  { path: '/prices', component: prices},
  { path: '/pictures', component: pictures},
  { path: '*', redirect: '/home'}
]
