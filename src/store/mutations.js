import func from  './functions'
var active = state => state.active

export default {
  enableTableButton: (state, payload) => {
    // state.tableButtonDisabled = true;
    state.tableButtonDisabled = payload == 0 ? true : false;
    // console.log(payload);
  },
  tempTableNumber: (state, payload) => {
    state.tableNumber = payload;
    // console.log(payload);
  },
  assignTable : (state) => {
    func.assign(state);
    // alert(state.tableNumber)
  },
  assignTable22: (state) => {

    var val = state.tableNumber;
    // var table = {"order_id": state.active, "table": state.tableNumber};
    // for(var i in state.allCarts) {
    //   if(state.allCarts[i].order_id == state.active) {
    //     if(state.allCarts[i].table != '') {
    //       state.assignedTables.splice(state.assignedTables.indexOf(state.allCarts[i].table,1));
    //     }
    //     state.allCarts[i].table = state.tableNumber;
    //     state.assignedTables.push(state.tableNumber);
    //   }
    // }
    for (var i in state.allCarts) {
    // state.allCarts.forEach( bill => {
      if(state.allCarts[i].order_id === state.active) {
        if(state.allCarts[i].table != '') {
          state.assignedTables.splice(state.assignedTables.indexOf(state.allCarts[i].table,1));
        }
        state.allCarts[i].table = val;
        // bill.table = payload;
        // console.log('payload : '+ state.allCarts[i].table);
        state.assignedTables.push(state.allCarts[i].table);
        // console.log('assigned tables :' + state.assignedTables);
        // console.log('bill table :' + bill.table );
        // console.log(state.allCarts);
        // return;
        // return;
        // break;
        // state.dialog = false;
        // console.log(state.dialog);
      }
    }
    state.dialog = false;
    // console.log(payload);
  },
  checkTableAssignment: (state) => {
    state.allCarts.forEach( bill => {
      if(bill.order_id == state.active) {
        if(bill.table != '') {
          state.currTable = bill.table;
        } else {
          state.currTable = '';
        }
      }
    })
  },
  assignTable_old: state => {
    var table = {"order_id": state.active, "table": state.tableNumber};
    console.log(table);
      // console.log('length of pairs: ' + state.pairs.length);
      if(state.pairs.length === 0) { // if a table has NOT been assigned
        console.log('all tables free');
          state.pairs.push(table); // assign table to current order
          // state.allTables.splice(payload, 1);
          state.assignedTables.push(state.tableNumber); // store table number
          console.log(state.currTable);

      }
      else { // if a table has been assigned
        state.pairs.forEach( pair => {
          if(pair.order_id === state.active) { // if current order already has a table assigned
            state.pairs.splice(state.pairs.indexOf(pair, 1)); // remove table from current order before assigning a new table
            state.assignedTables.splice(state.assignedTables.indexOf(pair.table,1));
            // state.pairs.push(table); // assign a new table number
            // state.assignedTables.push(state.tableNumber); // add table to assigned
            // return;
          }
          state.pairs.push(table);
          state.assignedTables.push(state.tableNumber);
        })
      }
      state.tableButtonDisabled = true;
      state.tableNumber = '';
  },
  setUrl: (state) => {
    switch(location.hostname) {
      case 'localhost':
        state.url = 'http://localhost/nazs/';
        break;
      case '11.1.1.11':
        state.url = 'http://11.1.1.11/nazs/';
        break;
      case 'nazs.net':
        state.url = 'https://nazs.net/';
        break;
    }
  },
  removeSubmittedCart: (state) => {
    for(var i in state.allCarts) {
      if(state.allCarts[i].order_id === state.active) {
        state.allCarts.splice(state.allCarts.indexOf(state.allCarts[i]), 1);
        state.active = null;
        state.activeCart = [];
        state.total = 0;
        // console.log(state.allCarts.indexOf(state.allCarts[i]));
        // console.log(state.allCarts[i].order_id);
      }
    }
    // state.allCarts.forEach( cart => {
    //   if(cart.order_id === state.active) {
    //     state.allCarts.splice(state.allCarts.indexOf(cart, 1));
    //     state.active = null;
    //     state.activeCart = [];
    //     state.total = 0;
    //   }
    // })
  },
  showDialog: state => {
    state.dialog = true;
  },
  hideDialog: state => {
    state.dialog = false;
  },
  resetCurrTable: (state) => {
    state.currTable = '';
  },
  sortTables: (state) => {
    state.allTables.sort(function(a, b){return a-b});
  },
  restoreTable: (state) => {
    state.allTables.push(state.currTable);
  },
  removepairs: (state, payload) => {
    state.pairs.splice(payload, 1);
  },
  setMenus: (state, payload) => {
    state.menus = payload;
    state.menus.forEach( menu => {
      switch(menu.category) {
        case 'starters':
          state.starters.push(menu);
          break;
        case 'soups':
          state.soups.push(menu);
          break;
        case 'beef':
          state.beef.push(menu);
          break;
        case 'chicken duck':
          state.chicken_duck.push(menu);
          break;
        case 'rice':
          state.rice.push(menu);
          break;
      }
    })
  },
  setOrderIdB4Submit: (state) => {
    state.activeCart.forEach( item => {
      item.order_id = state.active;
    })
  },
  createBill: (state, payload) => {
    var newD = {"order_id": payload, "table": '', "cart" : []};
    state.allCarts.push(newD);
    state.cust = true;
    state.active = payload;
    // console.log();
  },
  setAllCarts: (state, payload) => {
    state.allCarts = payload;
    // console.log(payload);
    state.allCarts.forEach( cart => {
      cart.table = '';
      cart.cart = [];
      // if(state.pairs.length> 0 ) {
      //   state.pairs.forEach( pair => {
      //     if(pair.order_id == cart.order_id) {
      //       cart.table = pair.table;
      //     }
      //   })
      // }
    })
  },
  setCustomers: (state, payload) => {
    state.customers = payload;
    // console.log('setCustomers triggered.');
  },
  setActive: (state, payload) => {
    state.active = payload;
  },
  setCust: (state, payload) => {
    state.cust = payload;
  },
  checkTableAssignment_old: (state) => {
    var t = state;
    if(t.pairs.length > 0 ) {
      t.currTable = '';
      for (var i in t.pairs) {
        if(t.pairs[i].order_id === t.active) {
          t.currTable = t.pairs[i].table;
          console.log('active table : ' + t.currTable);
        }
      }
    }
  },
  displayCart: (state) => {
  // var t = this.$store.state;
    state.allCarts.forEach( cart => {
      if(cart.order_id === state.active) {
          state.activeCart = cart.cart;
      }
    })
  },
  updateActive: (state) => {
    state.allCarts.filter((each) => {
      if(each.order_id == active) {
        state.activeCart = each;
      }
    })

    // console.log('order id: ', state.activeCart.order_id);
    // for(var i in state.allCarts) {
    //   if(state.allCarts[i].order_id == state.active) {
    //     state.activeCart = state.allCarts[i];
    //     console.log(state.activeCart);
    //   }
    // }
  },
  addToCart: (state, payload) => {
    // console.log(':' + state.allCarts.order_id);
    // if(state.activeCart.length < 6) {

      var basket = {"id": payload[0], "item": payload[1], "price": payload[2], "qty": 1};

      for (var i in state.allCarts) {
        if(state.allCarts[i].order_id == state.active) {
          for(var j in state.allCarts[i].cart) {
            if(state.allCarts[i].cart[j].id === basket.id) {
              state.allCarts[i].cart[j].qty = state.allCarts[i].cart[j].qty + 1;
              return;
            }
          }
          state.allCarts[i].cart.push(basket);
          state.activeCart = state.allCarts[i].cart;
          state.cartLength = state.activeCart.length;
          // console.log(basket);
        }
      }
      return false;
    // }
    // else {
    //   alert('sample quota reached.');
    // }
  },
  addAll: (state) => {
    var subTotal = 0;
    for (var i in state.allCarts) {
      if(state.allCarts[i].order_id == state.active) {
        for (var j in state.allCarts[i].cart) {
          subTotal = subTotal + state.allCarts[i].cart[j].qty * state.allCarts[i].cart[j].price;
        }
      }
    }
    state.total =  subTotal;
    state.cartLength = state.activeCart.length;
  },
  getOpenOrder: (state) => {

  },
  logUserIn: state => {
    state.userIn = true;
  },
  add: (state, payload) => {
    for (var i in state.allCarts) {
      if(state.allCarts[i].order_id == state.active) {
        for(var j in state.allCarts[i].cart) {
          if(state.allCarts[i].cart[j].id === payload) {
            state.allCarts[i].cart[j].qty++;
            return;
          }
        }
      }
    }
  },
  minus: (state, payload) => {
    for (var i in state.allCarts) {
      if(state.allCarts[i].order_id == state.active) {
        for(var j in state.allCarts[i].cart) {
          if(state.allCarts[i].cart[j].id === payload) {
            if(state.allCarts[i].cart[j].qty == 1) {
              state.allCarts[i].cart.splice(j,1);
              return;
            }
            state.allCarts[i].cart[j].qty--;
            return;
          }

        }

      }
    }
  },
  del: (state, payload) => {

    for (var i in state.allCarts) {
      if(state.allCarts[i].order_id === state.active) {
        for(var j in state.allCarts[i].cart) {
          if(state.allCarts[i].cart[j].id === payload) {
              state.allCarts[i].cart.splice(j,1);
              return;
          }
        }
      }
    }

  },

  addToCart_old: (state, payload) => {
    // var basket = payload;
    var basket = {"id": payload[0], "item": payload[1], "price": payload[2], "qty": 1};
    for(var i in state.cart) {
      if(state.cart[i].id === basket.id) {
        state.cart[i].qty = state.cart[i].qty + 1;
        return;
      }
    }
    console.log('first');
    console.log(basket);
    // console.log(state.active);
    state.cart.push(basket);
  },
  addAll_old: (state) => {
    var subTotal = 0;
    var total = 0;
    for (var i in state.cart) {
      subTotal = subTotal + state.cart[i].qty * state.cart[i].price;
    }
    state.total =  subTotal;
    console.log(state.total);
  },
  add_old: (state, payload) => {
    for (var i in state.cart) {
      if(state.cart[i].id === payload) {
        state.cart[i].qty++;
        return;
      }
    }
  },
  minus_old: (state, payload) => {
    for (var i in state.cart) {
      if(state.cart[i].id === payload) {
        if(state.cart[i].qty == 1) {
          state.cart.splice(i, 1);
          return;
        }
        state.cart[i].qty--;
        return;
      }
    }
  },
  del_old: (state, payload) => {
    for (var i in state.cart) {
      if(state.cart[i].id === payload) {
        state.cart.splice(i, 1);
        return;
      }
    }
  }
}
