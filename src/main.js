import Vue from 'vue'
// import App from './App.vue'
import App from './admin'
// import VueRouter from 'vue-router'
// import Routes from './routes'
// import EsPromise from 'es6-promise'
// import { store } from './store/store'

// Vue.use(VueRouter);
Vue.filter('formatDate', function(value){
  var m = new Date(value).getMonth() + 1;
  switch(m) {
    case 1:
     m = 'Jan'; break;
    case 2:
     m = 'Feb'; break;
    case 3:
     m = 'Mar'; break;
    case 4:
     m = 'Apr'; break;
    case 5:
     m = 'May'; break;
    case 6:
     m = 'Jun'; break;
    case 7:
     m = 'Jul'; break;
    case 8:
     m = 'Aug'; break;
    case 9:
     m = 'Sep'; break;
    case 10:
     m = 'Oct'; break;
    case 11:
     m = 'Nov'; break;
    case 12:
     m = 'Dec'; break;
  }
  if(value == null) {
    value = '[use calendar above]';
    // this.$store.state.dateChosen = true;
  } else {
    value = '' + new Date(value).getDate() + ' ' + m + ' ' + new Date(value).getFullYear();
  }
  // return value = value == null ? "[use calendar]" : '' + new Date(value).getDate() + ' ' + m + ' ' + new Date(value).getFullYear();
  return value;
});
Vue.filter('toUpperCase', function(value) {
  return value.toUpperCase();
});
Vue.filter('twoPlaces', function(value) {
  return value.toFixed(2);
});
Vue.filter('snippet', function(value) {
  return value.slice(0,15);
});

Vue.filter('lineBreak', function(value) {
  return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
});

// custom directive
Vue.directive('rainbow', {
  bind(el, binding, vnode) {
    el.style.color = "#" + Math.random().toString().slice(2,8);
  }
}); // usage: <h2 v-rainbow>Test</h2>

Vue.directive('theme', {
  bind(el, binding, vnode) {
    if(binding.value == 'wide') {
      el.style.maxWidth= "900px";
    } else if(binding.value == 'narrow') {
      el.style.maxWidth = '560px';
    };
    if(binding.arg == 'column') {
      el.style.background = '#ddd';
      el.style.color = 'purple';
    }
  }
}); // usage: <table v-theme:column="'wide'">

// const router = new VueRouter({
//   routes: Routes,
//   mode: 'history'
// });

new Vue({
  el: '#app',
  render: h => h(App)
  // router,
  // store
})
